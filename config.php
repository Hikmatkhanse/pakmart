<?php
$path = dirname(__FILE__);

function url(){
  return 'http://localhost/pakmart/';
}

define('HOST', 'localhost'); // the IP of the database
define('DBNAME', 'shopping'); // the database name to be used
define('USERNAME', 'root'); // the username to be used with the database
define('PASSWORD', ''); // the password to be used with the username

require_once('connection.php');
require_once('functions.php');

?>
		