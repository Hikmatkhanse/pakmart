<?php include_once('config.php'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>online shopping mart</title>
<link type="text/css" rel="stylesheet" href="<?php echo url().'/assets/css/' ?>pagestyle.css">
<link rel="stylesheet" href="<?php echo url(); ?>font-awesome/css/fontawesome.css">
<link rel="stylesheet" href="<?php echo url(); ?>font-awesome/css/font-awesome.min.css">
<link href='https://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>

</head>

<body>
<div id="header">
	<img id="img-banner" src="<?php echo url().'/assets/imgs/' ?>Logo-4.png" ></img>

<div class="topnav-login">
<ul class="topnav-right">
		<li><a href="<?php echo url().'signIn.php' ?>"><span class="fa fa-sign-in fa-2x"></span> Sign In</a>			
		</li>
		<li><a href="<?php echo url().'/brands.php' ?>"><span class="fa fa-shopping-cart fa-2x" ></span> My chart</a></li>
		
	</ul>
	</div>
<div id="navigation">
	<ul class="topnav">
  		<li><a href="<?php echo url(); ?>"><span class="fa fa-home" ></span>Home</a></li>
  		<li><a href="<?php echo url().'/accessories.php' ?>"><span class="fa fa-cart-plus" ></span>Accessories</a></li>
  		<li><a href="<?php echo url().'/brands.php' ?>"><span class="fa fa-star-half-o"></span>Brands</a></li>
  		<li><form><input type="text" id="search" placeholder=" Search..." ></form></li>
  		<li><a href="<?php echo url().'/contactus.php' ?>"><span class="fa fa-phone"></span>Contact us</a></li>
    	<li><a href="<?php echo url().'/about.php' ?>"><span class="fa fa-question-circle-o"></span>About us</a></li>
		<li class="icon">
    <a href="javascript:void(0);" style="font-size:15px;" onclick="myFunction()">☰</a>
  </li>
	</ul>
</div>
</div>
<script>
function myFunction() {
    document.getElementsByClassName("topnav")[0].classList.toggle("responsive");
}
</script>
</body>
</html>