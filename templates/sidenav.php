<div class="sidebar">
	<ul>
  		<li><a href="#"><span class="fa fa-male"></span>Men's Fashion</a>
				<ul>
					<li><a href="<?php echo url()?>/sidebar pages/men cloths.php">Men cloths</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/men shoes.php">Men Shoes</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/men perfomes.php">Men Perfomes</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/men jackets.php">Men Jackets</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/men jeans.php">Men Jeans</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/men hoodies.php">Men Hoodies</a></li>
				
				</ul>
		</li>
  		<li>
  		 <a href="<?php echo url()?>/sidebar pages/#fashion"><span class="fa fa-female"></span>Women's Fashion</a>
          <ul>
					<li><a href="<?php echo url()?>/sidebar pages/women shoes.php">Women Shoes</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women shirts.php">Women shirts</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women jackets.php">Women Jackets</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women dresses.php">Women Dresses</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women jeans">Women Jeans</a></li>

				</ul></li>
  		<li><a href="<?php echo url()?>/sidebar pages/#Accessories"><span class="fa fa-cutlery"></span>Home & Living </a>
          <ul>
					<li><a href="<?php echo url()?>/sidebar pages/outdoor furniture.php">Outdoor Furniture</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/sofas.php">Sofas</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/table accessories.php">Table Accessories</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/furniture set.php">Furniture Set</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/kitchen dinning.php">kitchen & Dinning</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/lawn garden.php">Lawn & Garden</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/lighting.php">Lighting</a></li>
				
				</ul>
        </li>
  		<li><a href="<?php echo url()?>/sidebar pages/#Electronics"><span class="fa fa-tablet"></span>Mobile phone's & tablets</a>
          <ul>
					<li><a  href="<?php echo url()?>/sidebar pages/samsung.php">Samsung</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/huawei.php">Huawei</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/Q mobile.php">Q mobile</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/LG.php">LG</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/sony.php">Sony</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/lenovo.php">Lenovo</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/nokia.php">Nokia</a></li>
					
				</ul></li>
  		<li><a href="<?php echo url()?>/sidebar pages/#Home Decor"><span class="fa fa-television"></span>Appliances & TVs</a>
          <ul>
					<li><a href="<?php echo url()?>/sidebar pages/tv equipment.php">TV Equipment</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/camera.php">Camera</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/Electronic appliances.php">Kitchen Appliances</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/MP3 player.php">MP3 Player Equipments</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/other electronics.php">Other Electonics</a></li>
					
				</ul></li>
   		<li><a href="<?php echo url()?>/sidebar pages/#Contact us"><span class="fa fa-laptop"></span>computers & Laptops Acessories</a>
           <ul >
					<li><a href="<?php echo url()?>/sidebar pages/mouses.php">Mouses</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/chargers.php">Chargers</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/laptop bags.php">Laptops Bags</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/laptop cases.php">Laptop Cases</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/cooling systems.php">Cooling Systems</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/batteries.php">Batteries</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/RAM.php">RAM</a></li>
				</ul></li>
   		<li><a href="<?php echo url()?>/sidebar pages/#Contact us"><span class="fa fa-plug"></span>Mobile Acessories</a>
           <ul >
					<li><a href="<?php echo url()?>/sidebar pages/memory SD cards.php">Memory & SD cards</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/powerbanks.php">PowerBanks</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/smart watches.php">Smart Watches</a></li>
                    <li><a href="<?php echo url()?>/sidebar pages/headphones.php">Headphones</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/screen protectors.php">Screen Protectors</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/chargers for phones tablets.php">Charges For Phones & Tables</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/sim cards.php">Sim Cards</a></li>
                    <li><a href="<?php echo url()?>/sidebar pages/mobile cases covers.php">Mobile Cases & Covers</a></li>
				</ul></li>
  		<li><a href="<?php echo url()?>/sidebar pages/#Contact us"><span class="fa fa-diamond"></span>Jewelery & Watches</a>
          <ul>
					<li><a href="<?php echo url()?>/sidebar pages/men watches.php">Men Watches</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women watches.php">Women Watches</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/kid watches.php">Kid Watches</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women jewelry.php">Women Jewelry</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women rings.php">Women Rings</a></li>
					<li><a href="<?php echo url()?>/sidebar pages/women bracelets.php">Women Bracelets</a></li>
					
				</ul></li>
  	</ul>
</div>